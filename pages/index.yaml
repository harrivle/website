title:      Basic Unix for Engineers
icon:       fa-linux
navigation:
    - name: "Logistics"
      link: "#logistics"
      icon: "fa-cogs"
    - name: "Schedule"
      link: "#schedule"
      icon: "fa-calendar"
    - name: "Requirements"
      link: "#Requirements"
      icon: "fa-balance-scale"
    - name: "Resources"
      link: "#resources"
      icon: "fa-external-link"
internal:
external:
    tas:        'static/yaml/ta.yaml'
    schedule:   'static/yaml/schedule.yaml'
body:       |

    <div class="row" markdown="1">

    **CSE 20189** is the one of the core classes in the [Computer Science and
    Engineering] program at the [University of Notre Dame].  The purpose of the
    course is to introduce students to the Unix programming environment and to
    familiarize students with a wide variety of Unix commands and tools, shell
    scripting, and Python programming.  Although this is primarily a
    practically-oriented course, along the way students will explore concepts
    such as filesystems, processes, pipes, and network sockets.

    [Computer Science and Engineering]:     http://cse.nd.edu
    [University of Notre Dame]:             http://www.nd.edu

    </div>

    <img src="http://2.bp.blogspot.com/_9B6Bu7nHAiI/SNHeMZtVpgI/AAAAAAAAAGA/FOPesiKXjH4/S1600-R/logo.png" class="pull-right" style="margin-left: 10px;"/>

    <div class="row" markdown="1">

    Upon successful completion of this course, students will be able to:

    1. **Utilize** unix commands to navigate filesystems, edit files, manage
    processes, explore system and network properties, produce documents and
    plots, and manipulate multimedia files.

    2. **Employ** Unix development tools to compile, link, build, debug, trace,
    profile, and test software applications.

    3. **Install** Unix software from source distributions and using package
    managers.

    4. **Compose** shell scripts to automate tasks.

    5. **Construct** regular expressions and small programs to filter and
    process a variety of datasets.

    6. **Develop** basic Python applications that utilize system calls that
    manipulate files, control processes, and communicate over pipes and
    sockets.

    7. **Discuss** the core tenets of the "Unix Philosophy" and how it is
    applied to modern software development.

    8. **Describe** how processes interact with each other via various forms of
    inter-process communication such as files, pipes, and sockets.

    </div>

    <div class="row">
        <div class="page-header">
            <h2 id="logistics"><i class="fa fa-cogs"></i> Logistics</h2>
        </div>
    </div>

    <div class="row">
    <div class="col-md-6">
    <div class="row">
    <div class="col-md-6">
        <h3>Class Information</h3>
        <dl>
            <dt><i class="fa fa-clock-o"></i> Lecture</dt>
            <dd><strong>M/W/F</strong> 12:50 PM - 1:40 PM</dd>

            <dt><i class="fa fa-home"></i> Location</dt>
            <dd>102 DeBartolo Hall</dd>

            <dt><i class="fa fa-envelope"></i> Mailing List (Class)</dt>
            <dd><a href="mailto:cse-20189-sp16-class-group@nd.edu">cse-20189-sp16-class-group@nd.edu</a></dd>

            <dt><i class="fa fa-envelope"></i> Mailing List (Staff)</dt>
            <dd><a href="mailto:cse-20189-sp16-staff-group@nd.edu">cse-20189-sp16-staff-group@nd.edu</a></dd>

            <dt><i class="fa fa-slack"></i> Slack</dt>
            <dd><a href="https://nd-cse.slack.com/messages/cse-20189-sp16/">#cse-20189-sp16</a></dd>

            <dt><i class="fa fa-bitbucket"></i> Bitbucket</dt>
            <dd><a href="https://bitbucket.org/CSE-20189-SP16/">cse-20189-SP16</a></dd>
        </dl>
    </div>
    <div class="col-md-6">
        <h3>Instructor</h3>

        <dl>
            <dt><i class="fa fa-user"></i> Instructor</dt>
            <dd><a href="http://www3.nd.edu/~pbui/">Peter Bui</a> (<a href="mailto:pbui@nd.edu">pbui@nd.edu</a>)</dd>

            <dt><i class="fa fa-life-ring"></i> Office Hours</dt>
            <dd><strong>M/W/F</strong> 2:00 PM - 4:30 PM, and by <strong>appointment</strong></dd>

            <dt><i class="fa fa-briefcase"></i> Office Location</dt>
            <dd>350 Fitzpatrick Hall</dd>
        </dl>
    </div>
    </div>

    <div class="row">
        <div class="alert alert-info">
            <h4><i class="fa fa-question"></i> Help Protocol</h4>
            <ol class="list-inline">
                <li><em><i class="fa fa-lightbulb-o"></i> Think</em></li>
                <li><small><i class="fa fa-arrow-right"></i></small></li>
                <li><strong><i class="fa fa-comment"></i> IRC</strong></li>
                <li><small><i class="fa fa-arrow-right"></i></small></li>
                <li><em><i class="fa fa-lightbulb-o"></i> Think</em></li>
                <li><small><i class="fa fa-arrow-right"></i></small></li>
                <li><strong><i class="fa fa-envelope"></i> Email</strong></li>
                <li><small><i class="fa fa-arrow-right"></i></small></li>
                <li><em><i class="fa fa-lightbulb-o"></i> Think</em></li>
                <li><small><i class="fa fa-arrow-right"></i></small></li>
                <li><strong><i class="fa fa-briefcase"></i> Office</strong></li>
            </ol>
        </div>
    </div>
    </div>

    <div class="col-md-6">
        <h3>Teaching Assistants</h3>

        <div class="row">
          {% for ta in sorted(page.external['tas'], key=lambda ta: ta['name'].split()[-1]) %}
          <div class="col-md-4">
              <dl>
                <dt><i class="fa fa-user"></i> Teaching Assistant</dt>
                <dd>{{ ta['name'] }} (<a href="mailto:{{ ta['netid'] }}@nd.edu">{{ ta['netid'] }}@nd.edu</a>)</dd>
                <dt><i class="fa fa-life-ring"></i> Office Hours</dt>
                <dd>{{ ta.get('hours', 'TBD') }}</dd>
                <dt><i class="fa fa-briefcase"></i> Office Location</dt>
                <dd>{{ ta.get('location', 'TBD') }}</dd>
              </dl>
          </div>
          {% end %}
        </div>
    </div>
    </div>

    <div class="row">
        <div class="page-header">
            <h2 id="schedule"><i class="fa fa-calendar"></i> Schedule</h2>
        </div>
    </div>

    <style>
    table.schedule td.unit {
        background-color: #fff;
        border-right: 2px solid #ddd;
        text-align: right;
        width: 175px;
        vertical-align: middle;
    }
    </style>

    <div class="row">
        <table class="table condensed table-striped schedule">
            <thead>
                <tr>
                    <th class="text-center">Unit</th>
                    <th class="text-center">Date</th>
                    <th class="text-center">Topics</th>
                    <th class="text-center" style="width: 125px;">Assignment</th>
                </tr>
            </thead>
            <tbody>

                {% for theme in page.external['schedule'] %}
                {% if 'break' in theme['name'].lower() %}
                  <tr>
                  <td colspan="4" class="text-center text-strong" style="border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; padding: 4px;">{{ theme['name'] }}</td>
                  </tr>
                  {% continue %}
                {% end %}

                {% for index, day in enumerate(theme['days']) %}
                <tr>
                {% if index == 0 %}
                    <td rowspan="{{ len(theme['days']) }}" class="text-strong text-primary text-center unit">{{ theme['name'] }}</td>
                {% end %}
                    <td class="text-center">{{ day['date'] }}</td>
                    <td>
                    {{ day.get('topics', '') }}
                    {% for item in day.get('items', []) %}
                    {% if item['name'].lower().startswith('exam') %}
                      {% set item['label'] = 'danger' %}
                    {% else %}
                      {% set item['label'] = 'default' %}
                    {% end %}
                    {% if 'link' in item %}
                      <a class="label label-{{ item['label'] }}" href="{{ item['link'] }}">{{ item['name'] }}</a>
                    {% else %}
                      <span class="label label-{{ item['label'] }}">{{ item['name'] }}</span>
                    {% end %}
                    {% end %}
                    </td>
                    <td class="text-strong">
                    {% set assignment = day.get('assignment') %}
                    {% if assignment %}
                    {% set assignment_id = ''.join(assignment.lower().split()) %}
                    {% if assignment_id.startswith('reading') %}
                      {% set assignment_label = 'info' %}
                    {% else %}
                      {% set assignment_label = 'success' %}
                    {% end %}
                      <a href="{{ assignment_id }}.html" class="label label-{{ assignment_label }}">{{ assignment }}</a>
                    {% end %}
                    </td>
                </tr>
                {% end %}
                {% end %}

            </tbody>
        </table>
    </div>

    <div class="row">
        <div class="page-header">
            <h2 id="Requirements"><i class="fa fa-balance-scale"></i> Requirements</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
        <h3>Coursework</h3>
        <table class="table table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th class="text-center">Component</th>
                    <th class="text-center">Points</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><span class="label label-info">Readings</span> Weekly reading assignments and corresponding writing prompts.</td>
                    <td class="text-center">12 &times; 4 + 2</td>
                </tr>
                <tr>
                    <td><span class="label label-success">Homeworks</span> Weekly programming homework assignments.</td>
                    <td class="text-center">10 &times; 15</td>
                </tr>
                <tr>
                    <td><span class="label label-danger">Exams</span> Periodic examinations, covering material discussed in class.</td>
                    <td class="text-center">4 &times; 25</td>
                </tr>
                <tr>
                    <td class="text-strong text-right">Total</td>
                    <td class="text-strong text-center">300</td>
                </tr>
            </tbody>
        </table>
        </div>
        <div class="col-md-6">
        <h3>Grading</h3>
        <table class="table table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">Grade</th>
                    <th class="text-center">Points</th>
                    <th class="text-center">Grade</th>
                    <th class="text-center">Points</th>
                    <th class="text-center">Grade</th>
                    <th class="text-center">Points</th>
                </tr>
            </thead>
            <tbody>
                <tr class="success">
                    <td></td>
                    <td></td>
                    <td class="text-center text-strong">A</td>
                    <td class="text-center">280-300</td>
                    <td class="text-center text-strong">A-</td>
                    <td class="text-center">270-279</td>
                </tr>
                <tr class="info">
                    <td class="text-center text-strong">B+</td>
                    <td class="text-center">260-269</td>
                    <td class="text-center text-strong">B</td>
                    <td class="text-center">250-259</td>
                    <td class="text-center text-strong">B-</td>
                    <td class="text-center">240-249</td>
                </tr>
                <tr class="warning">
                    <td class="text-center text-strong">C+</td>
                    <td class="text-center">230-239</td>
                    <td class="text-center text-strong">C</td>
                    <td class="text-center">220-229</td>
                    <td class="text-center text-strong">C-</td>
                    <td class="text-center">210-219</td>
                </tr>
                <tr class="danger">
                    <td class="text-center text-strong">D</td>
                    <td class="text-center">180-209</td>
                    <td class="text-center text-strong">F</td>
                    <td class="text-center">0-179</td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="alert alert-info">
                <h4><i class="fa fa-bitbucket"></i> Bitbucket Repository</h4>
                <p>
                All your <b>Reading</b> and <b>Homework</b> assignments are to
                be submitted to your own <b>private</b> <a
                href="https://bitbucket.org">Bitbucket</a> repository <b>before
                the class period</b> on the day assigned in the schedule above.
                </p>
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-md-12">
          <h3>Policies</h3>
          <div class="row">
              <div class="col-md-4">
              <h4>Participation</h4>
              <p>
              Students are expected to attend and contribute regularly in class. This
              means answering questions in class, participating in discussions, and
              helping other students.
              </p>
              <p>
              Foreseeable absences should be discussed with the instructor ahead of time.
              </p>
              </div>
              <div class="col-md-4">
              <h4>Classroom Recording</h4>
              <p>
              Notre Dame has implemented an Echo360 classroom recording system.
              This system allows us to record and distribute lectures to you in
              a secure environment. You can watch these recordings on your
              computer, tablet, or smartphone. The recordings can be accessed
              within Sakai. Look for the tool labeled "Echo360 ALP" on the left
              hand side of the course.
              </p>
              <p>
              Because we will be recording in the classroom and/or using an
              active learning environment, your questions and comments may be
              recorded. (Video recordings typically only capture the front of
              the classroom.) If you have any concerns about your voice or
              image being recorded, please speak to me to determine an
              alternative means of participating. No content will be shared
              with individuals outside of your course without your permission
              except for faculty and staff that need access for support or
              specific academic purposes.
              </p>
              <p>
              These recordings are jointly copyrighted by the University of
              Notre Dame and your instructor. Posting them to other websites,
              including YouTube, Facebook, Vimeo, or elsewhere without express,
              written permission may result in disciplinary action and possible
              civil prosecution.
              </p>
              </div>
              <div class="col-md-4">
              <h4>Late Work</h4>
              <p>
              In the case of a serious illness or other excused absence, as defined by
              university policies, coursework submissions will be accepted late by the
              same number of days as the excused absence.
              </p>
              <p>
              Otherwise, there is a penalty of 25% per day late (except where noted). You
              may submit some parts of an assignment on time and some parts late. Each
              submission must clearly state which parts it contains; no part can be
              submitted more than once.
              </p>
              </div>
              <div class="col-md-4">
              <h4>Honor Code</h4>
              <p>
              All work that you submit must be your own. Collaboration is encouraged
              but must be disclosed by all parties. Print or online resources are
              allowed, but must be disclosed. However, you may not look at solutions
              from other current or past students, or any other source.
              </p>
              </div>
              <div class="col-md-4">
              <h4>Students with Disabilities</h4>
              <p>
              Any student who has a documented disability and is registered with
              Disability Services should speak with the professor as soon as possible
              regarding accommodations. Students who are not registered should contact
              the <a href="http://disabilityservices.nd.edu/">Office of Disabilities</a>.
              </p>
              </div>
          </div>
      </div>
    </div>

    <div class="row">
        <div class="page-header">
            <h2 id="resources"><i class="fa fa-external-link"></i> Resources</h2>
        </div>
    </div>

    <div class="row" markdown="1">
    <div class="col-md-4" markdown="1">
    ### Textbooks

    - [The Linux Command Line](http://linuxcommand.org/tlcl.php)
    - [Introduction to Linux](http://tldp.org/LDP/intro-linux/html/)

    ### Unix Tutorials

    - [Learning the Shell](http://linuxcommand.org/lc3_learning_the_shell.php)
    - [UNIX / Linux Tutorial for Beginners](http://www.ee.surrey.ac.uk/Teaching/Unix/)
    - [FreeBSD Handbook - UNIX Basics](https://www.freebsd.org/doc/handbook/basics.html)
    </div>
    <div class="col-md-4" markdown="1">
    ### Git

    - [Git-Scm](https://git-scm.com/)
    - [Pro Git](https://git-scm.com/book/en/v2)
    - [Atlassian Getting Started Git Tutorials](https://www.atlassian.com/git/tutorials)
    - [gittutorial](http://git-scm.com/docs/gittutorial)
    - [Code School - Try Git](https://try.github.io/levels/1/challenges/1)
    - [Git - Tutorial](http://www.vogella.com/tutorials/Git/article.html)
    - [Learn Git Branching](http://pcottle.github.io/learnGitBranching/)
    </div>
    <div class="col-md-4" markdown="1">
    ### Manual Pages

    - [Linux man pages online](http://man7.org/linux/man-pages/index.html)
    - [FreeBSD man pages](https://www.freebsd.org/cgi/man.cgi)
    - [POSIX Standard](http://pubs.opengroup.org/onlinepubs/9699919799/)
    </div>
    <div class="col-md-4" markdown="1">
    ### Software

    - [Homebrew](homebrew.html) **(Mac OS X)**
    </div>
    </div>
